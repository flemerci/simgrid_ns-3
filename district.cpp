#include <bits/stdc++.h>
#include "district.hpp"
#include "simgrid/s4u.hpp"
#include "aggregator.hpp"
#include "substation.hpp"
#include "smartmeter.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(district, "Messages specific for this s4u example");

std::string District::get_next_smartmeter_in_phase(Phase phase, int index) {
    unsigned int i = index;
    while (i < smartmeters.size() && smartmeters[i]->get_phase() != phase)
        i++;
    if (i < smartmeters.size())
        return smartmeters[i]->get_name() + "_token";
    else
        return get_next_smartmeter_in_phase(phase, 0);
}

void District::set_next_mailboxes() {
    for (unsigned int i = 0; i < smartmeters.size(); i++) {
        simgrid::s4u::Mailbox* next_mailbox = simgrid::s4u::Mailbox::by_name(get_next_smartmeter_in_phase(smartmeters[i]->get_phase(),i+1));
        smartmeters[i]->set_next_mailbox(next_mailbox);
    }
}

District::District(int id_, Config config) : id(id_) {

    simgrid::s4u::Engine* e = simgrid::s4u::Engine::get_instance();
    std::shuffle(fmu_input.begin(), fmu_input.end(), std::default_random_engine(rand()));

    aggregator = new Aggregator(id,
                                config.upper_current_threshold_ka,
                                config.log_dir,
                                e->host_by_name("aggregator_" + std::to_string(id)),
                                config.management);

    substation = new Substation(start_time,
                                10,
                                config.message_size_bytes,
                                config.log_dir,
                                e->host_by_name("substation_" + std::to_string(id)),
                                simgrid::s4u::Mailbox::by_name(aggregator->get_name() + "_data_substation"));

    for (int i = 0; i < config.nb_sheddable_houses; i++) {
        smartmeters.push_back(new SmartMeter(start_time,
                                             config.sampling_period_s,
                                             config.message_size_bytes,
                                             true,
                                             config.flexible_loads_dir,
                                             config.non_flexible_loads_dir,
                                             fmu_input[i].first,
                                             config.log_dir,
                                             aggregator->get_name(),
                                             e->host_by_name("smart_meter_" + std::to_string(id) + "_" + std::to_string(i)),
                                             fmu_input[i].second,
                                             config.management));
    }

    /// if we are in a decentralized scenario then aggregator needs to know
    /// the firsts smartmeter of each ring to sends tokens
    if (config.management == Management::DECENTRALIZED) {
        aggregator->set_first_mailbox_per_ring(get_next_smartmeter_in_phase(Phase::A),
                                               get_next_smartmeter_in_phase(Phase::B),
                                               get_next_smartmeter_in_phase(Phase::C));
        set_next_mailboxes();
    }

    for (int i = config.nb_sheddable_houses; i < nb_total_houses; i++) {
        smartmeters.push_back(new SmartMeter(start_time,
                                             config.sampling_period_s,
                                             config.message_size_bytes,
                                             false,
                                             config.flexible_loads_dir,
                                             config.non_flexible_loads_dir,
                                             fmu_input[i].first,
                                             config.log_dir,
                                             aggregator->get_name(),
                                             e->host_by_name("smart_meter_" + std::to_string(id) + "_" + std::to_string(i)),
                                             fmu_input[i].second,
                                             config.management));
    }
}
